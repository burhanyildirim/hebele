<?php 

include 'config/config.php';
include 'panel-left.php';
 ?>

<!DOCTYPE html>
<html>
 <head>
  <title>Webslesson Tutorial</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

  
  <style>
   #box
   {
    width:600px;
    background:gray;
    color:white;
    margin:0 auto;
    padding:10px;
    text-align:center;
   }
  </style>
 </head>
 <body>
  <div class="rightBottom">
      <div class="descArea">
  <div class="container">
   <br />
   <h3 align="center" class="descAreaTitle" >Ürün Güncelle</h3><br />
   
   <?php
   if(mysqli_num_rows($result1) > 0)
   {
   ?>
   <div class="table-responsive">
    <table class="table table-bordered">
     <tr>
      <th>Item</th>
      <th>Spor ID</th>
      <th>Panopli ID</th>
      <th>Ürün Adı</th>
      <th>Fiyat</th>
      <th>Cinsiyet</th>
      <th>Yaş</th>
      <th>Fotoğraf</th>
      <th>Vücut Bölümü</th>
      <th>Update</th>
     </tr>
   <?php
    while($row = mysqli_fetch_array($result1))
    {
   ?>
     <tr id="<?php echo $row["item_code"]; ?>" >
      <td><input type="text" name="item_code" value="<?php echo $row["item_code"]; ?>" required></td>
      <td><input type="text" name="sport_id" value="<?php echo $row["sport_id"]; ?>" required></td>
      <td><input type="text" name="panopli_id" value="<?php echo $row["panopli_id"]; ?>" required></td>
      <td><input type="text" name="name" value="<?php echo $row["name"]; ?>" required></td>
      <td><input type="text" name="cost" value="<?php echo $row["cost"]; ?>" required></td>
      <td><select name="gender">
        <option selected="" disabled="" hidden="">Cinsiyet Seçiniz</option>
                <option value="1">Erkek</option>
                <option value="2">Kadın</option>
              </select></td>
      <td><select name="age">
        <option selected="" disabled="" hidden="">Yaş Seçiniz</option>
                <option value="1">Çocuk</option>
                <option value="2">Yetişkin</option>
              </select></td>
      <td> <input type="file" name="image"  /></td>
      <td><input type="text" name="body_section" value="<?php echo $row["body_section"]; ?>" required></td>
      <td> <input type="submit" name="güncellebutonu" value="Update" ></td>
      
     </tr>
   <?php 
    }
   ?>
    </table>
   </div>

   <?php
   }
   ?>
   
</div><!--rightBottom-->
  </div><!--rightArea-->

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="node_modules/fontawesome/index.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


   
 </body>
</html>