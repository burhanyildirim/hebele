<?php 

include 'config/config.php';
include 'panel-left.php';
 ?>

<!DOCTYPE html>
<html>
 <head>
  <title>Webslesson Tutorial</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

  
  <style>
   #box
   {
    width:600px;
    background:gray;
    color:white;
    margin:0 auto;
    padding:10px;
    text-align:center;
   }
  </style>
 </head>
 <body>
  <div class="rightBottom">
      <div class="descArea">
  <div class="container">
   <br />
   <h3 align="center" class="descAreaTitle" >Silmek İstediğiniz Ürünleri Seçiniz.</h3><br />
   
   <?php
   if(mysqli_num_rows($result1) > 0)
   {
   ?>
   <div class="table-responsive">
    <table class="table table-bordered">
     <tr>
      <th>Item</th>
      <th>Spor ID</th>
      <th>Panopli ID</th>
      <th>Ürün Adı</th>
      <th>Fiyat</th>
      <th>Cinsiyet</th>
      <th>Yaş</th>
      <th>Fotoğraf</th>
      <th>Vücut Bölümü</th>
      <th>Delete</th>
     </tr>
   <?php
    while($row = mysqli_fetch_array($result1))
    {
   ?>
     <tr id="<?php echo $row["item_code"]; ?>" >
      <td><?php echo $row["item_code"]; ?></td>
      <td><?php echo $row["sport_id"]; ?></td>
      <td><?php echo $row["panopli_id"]; ?></td>
      <td><?php echo $row["name"]; ?></td>
      <td><?php echo $row["cost"]; ?></td>
      <td><?php echo $row["gender"]; ?></td>
      <td><?php echo $row["age"]; ?></td>
      <td><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image'] ).'" height="50" width="50" class="img-thumnail" />';?></td>
      <td><?php echo $row["body_section"]; ?></td>
      <td><input type="checkbox" name="customer_id[]" class="delete_customer" value="<?php echo $row["item_code"]; ?>" /></td>
     </tr>
   <?php 
    }
   ?>
    </table>
   </div>

   <?php
   }
   ?>
   <div align="center">
    <button type="button" name="btn_delete" id="btn_delete" class="btn btn-success">Delete</button>
   </div>
</div><!--rightBottom-->
  </div><!--rightArea-->

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="node_modules/fontawesome/index.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


   <script>
$(document).ready(function(){
 
 $('#btn_delete').click(function(){
  
  if(confirm("Are you sure you want to delete this?"))
  {
   var item_code = [];
   
   $(':checkbox:checked').each(function(i){
    item_code[i] = $(this).val();
   });
   
   if(item_code.length === 0) //tell you if the array is empty
   {
    alert("Please Select atleast one checkbox");
   }
   else
   {
    $.ajax({
     url:'./config/config.php',
     method:'POST',
     data:{item_code:item_code},
     success:function()
     {
      for(var i=0; i<item_code.length; i++)
      {
       $('tr#'+item_code[i]+'').css('background-color', '#ccc');
       $('tr#'+item_code[i]+'').fadeOut('slow');
      }
     }
     
    });
   }
   
  }
  else
  {
   return false;
  }
 });
 
});
</script>
 </body>
</html>