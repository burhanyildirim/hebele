-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 13 Tem 2018, 19:52:39
-- Sunucu sürümü: 10.1.33-MariaDB
-- PHP Sürümü: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `panopli_project`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `body`
--

CREATE TABLE `body` (
  `id` int(5) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `body`
--

INSERT INTO `body` (`id`, `name`) VALUES
(1, 'a');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `panopli`
--

CREATE TABLE `panopli` (
  `id` int(5) NOT NULL,
  `sport_id` int(5) NOT NULL,
  `name` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `panopli`
--

INSERT INTO `panopli` (`id`, `sport_id`, `name`) VALUES
(1, 30, 'qw'),
(2, 30, 'qwsxc'),
(3, 30, 'dasasd'),
(4, 30, 'asdasd'),
(5, 30, 'wwwwww'),
(6, 1, 'a'),
(222, 30, 'adsasd'),
(232, 30, 'adsasd'),
(234, 30, 'bm'),
(555, 30, 'qqq'),
(666, 30, 'mmmm'),
(2222, 30, 'ekle'),
(3123, 29, 'dsf'),
(123123, 30, 'burhan');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `products`
--

CREATE TABLE `products` (
  `item_code` int(15) NOT NULL,
  `sport_id` int(5) NOT NULL,
  `panopli_id` int(5) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `cost` int(5) DEFAULT NULL,
  `gender` int(5) DEFAULT NULL,
  `age` int(5) NOT NULL,
  `image` blob,
  `body_section` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `products`
--

INSERT INTO `products` (`item_code`, `sport_id`, `panopli_id`, `name`, `cost`, `gender`, `age`, `image`, `body_section`) VALUES
(1, 0, 1, 'a', 1, 0, 0, NULL, 1),
(2, 0, 2, 's', 2, 0, 0, NULL, 1),
(3, 0, 1, 'e', 1, 0, 0, NULL, 1),
(12, 1, 1, 'sss', 1, 0, 0, '', 1),
(22, 1, 1, 'sssssss', 1, 0, 0, '', 1),
(33, 1, 1, 'ccc', 1, 0, 0, '', 1),
(3333, 1, 1, 'ee', 1, 0, 0, '', 1),
(6543, 1, 1, 'fvcvd', 1, 0, 0, '', 1),
(23213, 1, 1, 'ss', 1, 0, 0, '', 1),
(87654, 1, 1, 'uytr1', 1, 0, 0, '', 1),
(123123, 1, 1, 'dsdfs', 1, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sports`
--

CREATE TABLE `sports` (
  `id` int(5) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `sports`
--

INSERT INTO `sports` (`id`, `name`) VALUES
(0, 'sssss'),
(1, 'qweqwe'),
(3, 'gggggg'),
(29, 'yürüyüş'),
(30, 'bbbb'),
(33, 'rrrr'),
(75, 'asdwwq'),
(76, 'asdwwq'),
(77, 'wwwwwww'),
(78, 'wwwwwww'),
(79, 'eeeee'),
(80, 'eeeee');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `uid` int(5) NOT NULL,
  `name` varchar(25) NOT NULL,
  `mail` varchar(25) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`uid`, `name`, `mail`, `password`) VALUES
(1, 'hahhaha', 'adsasdas', 'asdasdasd'),
(2, 'hahhaha', 'adsasdas', 'asdasdasd');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `body`
--
ALTER TABLE `body`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `panopli`
--
ALTER TABLE `panopli`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Spor ID` (`sport_id`);

--
-- Tablo için indeksler `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`item_code`),
  ADD KEY `panopli_id` (`panopli_id`),
  ADD KEY `SportID` (`sport_id`),
  ADD KEY `body` (`body_section`);

--
-- Tablo için indeksler `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `body`
--
ALTER TABLE `body`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `panopli`
--
ALTER TABLE `panopli`
  ADD CONSTRAINT `Spor ID` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`id`);

--
-- Tablo kısıtlamaları `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `SportID` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`id`),
  ADD CONSTRAINT `body` FOREIGN KEY (`body_section`) REFERENCES `body` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`panopli_id`) REFERENCES `panopli` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
