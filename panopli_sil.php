<?php 

include 'config/config.php';
include 'panel-left.php';
 ?>

<!DOCTYPE html>
<html>
 <head>
  <title>Webslesson Tutorial</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

  
  <style>
   #box
   {
    width:600px;
    background:gray;
    color:white;
    margin:0 auto;
    padding:10px;
    text-align:center;
   }
  </style>
 </head>
 <body>
  <div class="rightBottom">
      <div class="descArea">
  <div class="container">
   <br />
   <h3 align="center" class="descAreaTitle">Silmek İstediğiniz Panoplileri Seçiniz.</h3><br />
   
   <?php
   if(mysqli_num_rows($result2) > 0)
   {
   ?>
   <div class="table-responsive">
    <table class="table table-bordered">
     <tr>
      <th>Panopli ID</th>
      <th>Spor ID</th>
      <th>Panopli Adı</th>
      <th>Delete</th>
     </tr>
   <?php
    while($row = mysqli_fetch_array($result2))
    {
   ?>
     <tr id="<?php echo $row["id"]; ?>" >
      <td><?php echo $row["id"]; ?></td>
      <td><?php echo $row["sport_id"]; ?></td>
      <td><?php echo $row["name"]; ?></td>
      <td><input type="checkbox" name="customer_id[]" class="delete_customer" value="<?php echo $row["id"]; ?>" /></td>
     </tr>
   <?php
    }
   ?>
    </table>
   </div>

   <?php
   }
   ?>
   <div align="center">
    <button type="button" name="btn_delete" id="btn_delete" class="btn btn-success">Delete</button>
   </div>
</div><!--rightBottom-->
  </div><!--rightArea-->

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="node_modules/fontawesome/index.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


   <script>
$(document).ready(function(){
 
 $('#btn_delete').click(function(){
  
  if(confirm("Are you sure you want to delete this?"))
  {
   var id = [];
   
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    alert("Please Select atleast one checkbox");
   }
   else
   {
    $.ajax({
     url:'./config/config.php',
     method:'POST',
     data:{id:id},
     success:function()
     {
      for(var i=0; i<id.length; i++)
      {
       $('tr#'+id[i]+'').css('background-color', '#ccc');
       $('tr#'+id[i]+'').fadeOut('slow');
      }
     }
     
    });
   }
   
  }
  else
  {
   return false;
  }
 });
 
});
</script>
 </body>
</html>