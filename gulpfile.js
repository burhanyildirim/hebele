var gulp = require("gulp");
var sass = require("gulp-sass")
const autoprefixer = require('gulp-autoprefixer');
gulp.task("newCss", function () {
  return gulp.src("./scss/main.scss")
    .pipe(sass())
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(gulp.dest("./css"));
})


gulp.task("izle", function(){
	gulp.watch('./scss/**/*.scss', ['newCss'])
})



gulp.task('default', ['izle', 'newCss'])