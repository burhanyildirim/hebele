<?php 

include 'config/config.php';
include 'panel-left.php';
 ?>


<!-- Modal -->

		<div class="rightBottom">
			<div class="descArea">
				<h3 align="center" class="descAreaTitle">Ürün Ekle</h3><!--descAreaTitle-->
				<div class="desContent">
					<form action="" method="post" enctype="multipart/form-data">
					
					<div class="labels">
							<div>
							<span>Item Code :</span>
							</div>
						<td><input type="text" name="item_code" required>
                  		</td>
					</div><!--labels-->

					<div class="labels">
							<div>
							<span>Spor ID :</span>
							</div>
						<td><input type="text"  name="products_sport_id" required>
                  		</td>
					</div><!--labels-->

					<div class="labels">
							<div>
							<span>Panopli ID :</span>
							</div>
						<td><input type="text" name="products_panopli_id" required>
                  		</td>
					</div><!--labels-->

					<div class="labels">
							<div>
							<span>Ürün Adı :</span>
							</div>
						<td><input type="text" name="products_name" required>
                  		</td>
					</div><!--labels-->

					<div class="labels">
							<div>
							<span>Fiyat :</span>
							</div>
						<td><input type="text" name="cost" required>
                  		</td>
					</div><!--labels-->

					<div class="labels">
							<div>
							<span>Cinsiyet :</span>
							</div>
						<select name="gender">
				<option selected="" disabled="" hidden="">Cinsiyet Seçiniz</option>
								<option value="1">Erkek</option>
								<option value="2">Kadın</option>
							</select>

					</div><!--labels-->
					
					<div class="labels">
							<div><span>Yaş :</span></div>
						<select name="age">
				<option selected="" disabled="" hidden="">Yaş Seçiniz</option>
								<option value="1">Çocuk</option>
								<option value="2">Yetişkin</option>
							</select>
					</div><!--labels-->
					
					<div class="labels">
							<div>
								<span>Vücut Bölümü :</span>
							</div>
							<td><input type="text" name="body" required>
							</td>
					</div><!--labels-->

					<div class="labels">
							<div>
							<span>Fotoğraf :</span>
							</div>
						
                     <input type="file" name="image"  />

					</div><!--labels-->
				
	
					<div class="buttons">
						<button type="submit" class="st-btn st-btn-yellow st-btn-right" id="productseklebutonu" name="productseklebutonu" data-toggle="modal" data-target="#myModal">Ekle</button>

					</div><!--buttons-->
					</form>
					<div class="gorselInfo"></div><!--gorselInfo-->
				</div><!--desContent-->
				
			</div><!--descArea-->

		</div><!--rightBottom-->
	</div><!--rightArea-->

</div><!--wrapper-->
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="node_modules/fontawesome/index.js"></script>
</body>
</html>