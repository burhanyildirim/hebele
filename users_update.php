<?php 

include 'config/config.php';
include 'panel-left.php';

 ?>

<!DOCTYPE html>
<html>
 <head>
  <title>Webslesson Tutorial</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />

  
  <style>
   #box
   {
    width:600px;
    background:gray;
    color:white;
    margin:0 auto;
    padding:10px;
    text-align:center;
   }
  </style>
 </head>
 <body>
  <div class="rightBottom">
      <div class="descArea">
  <div class="container">
   <br />
   <h3 align="center" class="descAreaTitle">Kullanıcı Güncelle</h3><br />
   
   <?php
   if(mysqli_num_rows($result) > 0)
   {
   ?>
   <div class="table-responsive">
    <table class="table table-bordered">
     <tr>
      <th>Kullanıcı Adı</th>
      <th>Email</th>
      <th>Şifre</th>
      <th>Delete</th>
     </tr>
   <?php
    while($row = mysqli_fetch_array($result))
    {
   ?>
     <tr id="<?php echo $row["id"]; ?>" >
      <td><input type="text" name="name" value="<?php echo $row["name"]; ?>" required></td>
      <td><input type="text" name="mail" value="<?php echo $row["mail"]; ?>" required></td>
      <td><input type="text" name="password" value="<?php echo $row["pwd"]; ?>" required></td>
      <td> <button type="button" name="btn_update" id="btn_update" class="btn btn-success">Update</button></td>
     </tr>
   <?php
    }
   ?>
    </table>
   </div>

   <?php
   }
   ?>
   
</div><!--rightBottom-->
  </div><!--rightArea-->

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="node_modules/fontawesome/index.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


  
 </body>
</html>
