<?php include './config/config.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Decathlon Admin Panel</title>
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
	
<div id="wrapper">

	<div class="leftMenu">
			<div class="leftMenuArea">
				<div class="logo">
					<a href="panel.php">
						<img src="https://www.decathlon.com.tr/skins/images/specific-country/components/DktLogo/decathlonlogo.png" alt="Decathlon logo">
					</a>
				</div><!--logo-->
			</div><!--leftMenuArea-->
			<div class="leftMenuArea">
				<div class="LeftMenuTitle">Admin Panel</div>
				<ul class="menuL">
				    	<li><a href='panopli_ekle.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Panopli Ekle</a></li>
				    	<li><a href='panopli_sil.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Panopli Sil</a></li>
				    	<li><a href='panopli_update.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Panopli Güncelle</a></li>
				    	<li><a href='spor_ekle.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Spor Ekle</a></li>
				    	<li><a href='spor_sil.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Spor Sil</a></li>
				    	<li><a href='sport_update.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Spor Güncelle</a></li>
				    	<li><a href='product_ekle.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Ürün Ekle</a></li>
				    	<li><a href='product_sil.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Ürün Sil</a></li>
				    	<li><a href='product_update.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Ürün Güncelle</a></li>
				    	<li><a href='users_ekle.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Kullanıcı Ekle</a></li>
				    	<li><a href='users_sil.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Kullanıcı Sil</a></li>
				    	<li><a href='users_update.php' class='<?php echo (($thisPage == "Kampanya Detay") ? 'active':'')?>'>Kullanıcı Güncelle</a></li>
				    	
				    	
				    	
				</ul>
			</div><!--leftMenuArea-->



	</div><!--leftMenu-->
